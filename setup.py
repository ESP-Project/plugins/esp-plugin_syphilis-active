'''
                                  ESP Health
                          Diabetes Disease Definition
                             Packaging Information
                                  
@author: Bob Zambarano <bzambarano@commoninf.com>, Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics, Inc. http://www.commoninf.com
@contact: http://esphealth.org
@copyright: (c) 2017 Commonwealth Informatics
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-syphilis-active',
    version = '1.2',
    author = 'Jeff Andre',
    author_email = 'jandre@commoninf.com',
    description = 'Syphilis Active disease definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'syphilis algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = syphilis_active:disease_definitions
        event_heuristics = syphilis_active:event_heuristics
    '''
    )
